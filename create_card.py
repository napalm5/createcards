import glob
import os
import sys
import json
import jinja2
import subprocess
from os.path import join as pjoin
from os.path import *
from itertools import product

from modules import varContainer

DATA_DIR = './data/'
TEMPLATES_DIR = './templates/'
LATEX_DIR = './latex/'
REPORTS_DIR = './reports/'
ROOT_DIR = os.getcwd()
CITY = sys.argv[1]

# Load variables
container = varContainer()
for plot_file in glob.glob(pjoin(DATA_DIR,CITY,'*jpg')):
    filename, ext = splitext(basename(plot_file))
    setattr(container,filename,abspath(plot_file))
    
with open(pjoin(DATA_DIR,CITY,"foot_rank.json")) as foot, open(pjoin(DATA_DIR,CITY,"bike_rank.json")) as bike:
    foot_data = json.load(foot)
    bike_data = json.load(bike)
    setattr(container, "foot_rank", foot_data['rank'])
    setattr(container, "foot_time", foot_data['metric'])
    setattr(container, "bike_rank", bike_data['rank'])
    setattr(container, "bike_time", bike_data['metric'])
            
print(container.__dict__)


# Load template and render
REPORT_NAME = f"report_{CITY}"
REPORT_LATEX_FILE = REPORT_NAME+'.tex'
REPORT_FILE = REPORT_NAME+'.pdf'

templateLoader = jinja2.FileSystemLoader(searchpath=TEMPLATES_DIR)
templateEnv = jinja2.Environment(loader=templateLoader,variable_start_string = '\VAR{',variable_end_string = '}',
                                 block_start_string = '\BLOCK{',
                                 block_end_string = '}',
                                 comment_start_string = '\#{',
                                 comment_end_string = '}',
                                 line_statement_prefix = '%-',
                                 line_comment_prefix = '%#',
                                 trim_blocks = True,
                                 autoescape = False)
TEMPLATE_FILE = "report.jinja.tex"
template = templateEnv.get_template(TEMPLATE_FILE)
# Only works if names in template are the same as in the file structure
# Might be better to write them manually
variables = container.__dict__
outputText = template.render(city=CITY.capitalize(),
                             **variables
                             )

# Save template and compile
if not os.path.exists(LATEX_DIR+CITY):
    os.mkdir(LATEX_DIR+CITY)
with open(os.path.join(LATEX_DIR,CITY,REPORT_LATEX_FILE),'w') as output:
    output.write(outputText)

x = subprocess.run(['pdflatex',
                    '-output-directory', LATEX_DIR+CITY,
                    '-jobname', REPORT_NAME,
                    os.path.join(LATEX_DIR,CITY,REPORT_LATEX_FILE)])
y = subprocess.run(['mv',
                    os.path.join(LATEX_DIR,CITY,REPORT_FILE),
                    os.path.join(REPORTS_DIR,REPORT_FILE)])
