test:
	python create_card.py milano

all:
	bash create_all_cards.sh
clean:
	rm -r reports/* latex/*
archive:
	git archive --format zip --output createcards.zip master
